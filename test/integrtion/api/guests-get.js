import {expect} from 'chai'

const server = require('../../../server'),
    request = require('supertest')(server)

describe('Product GET API', () => {
    it('should get guests list', (done) => {
        request
            .get('/api/guests/')
            .set('Accept', 'application/json')
            .end((err, response) => {
                if (err) done(err)
                expect(response.statusCode).to.equal(200)
                done()
            })
    })
    it('shoul return 201 when add a Guest', done => {
        request
            .post('/api/guests/')
            .send({
                email: 'anEmail@mail.com'
            })
            .set('Accept', 'application/json')
            .end((err, response) => {
                console.log('error' + err)
                console.log('response' + response)
                if (err) done(err)
                expect(response.statusCode).to.equal(201)
                done()
            })
    })
})
