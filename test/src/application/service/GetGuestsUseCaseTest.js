import {expect} from 'chai'
import {GetGuestsUseCase} from '../../../../src/application/service/getGuestsUseCase'
import sinon from 'sinon'

describe('GetGuestsUseCase should return list of Gutess', () => {
        const repository = {
            get: () => null
        }
        it('Should initialize UseCase correctly', () => {
            const getGuestsUseCase = new GetGuestsUseCase({repository})
            expect(getGuestsUseCase).not.to.be.undefined
        })
        it('Should delegate on repository', () => {
            const repositorySpied = sinon.spy(repository, "get")
            const getGuestsUseCase = new GetGuestsUseCase({repository})
            getGuestsUseCase.get()
            expect(repositorySpied.calledOnce).to.be.true
        })
    }
)