import {AddGuestUseCase} from '../../../../src/application/service/addGuestUseCase'
import sinon from 'sinon'
import {expect} from 'chai'

describe('AddGuestUseCase should insert a Guest in the repository', () => {
    it('Should delegate to Repository', () => {
        const repository = {
            add: () => null
        }
        const repositoyrSpied = sinon.spy(repository, "add");
        const addGuestUseCase = new AddGuestUseCase({repository})
        addGuestUseCase.add()
        expect(repositoyrSpied.calledOnce).to.be.true
    })
})