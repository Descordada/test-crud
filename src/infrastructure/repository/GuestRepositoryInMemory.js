import {GuestRepository} from '../../domain/repository/GuestRepository'
class GuestRepositoryInMemory extends GuestRepository {

    constructor() {
        super()
        this.list = []
    }
    get() {
        return this.list
    }

    add(guest) {
        if( guest === null || guest === undefined) {
            throw Error('guest could not be null')
        }
        console.log('GuestRepositoryInMemory:add' + JSON.stringify(guest))
        this.list.push(guest)
    }
}

export {GuestRepositoryInMemory}