class AddGuestUseCase {
    constructor({repository}) {
        this.repository = repository
    }

    add(guest) {
        this.repository.add(guest)
    }
}

export {AddGuestUseCase}