import express from 'express'
import {GetGuestsUseCase} from "./src/application/service/getGuestsUseCase";
import {GuestRepositoryInMemory} from "./src/infrastructure/repository/GuestRepositoryInMemory.js";
import {AddGuestUseCase} from "./src/application/service/addGuestUseCase.js";
const port = 3000
const app = express();

const repository = new GuestRepositoryInMemory()
const getGuestsUseCase = new GetGuestsUseCase({repository})
const addGuestUseCase = new AddGuestUseCase({repository })

app.listen(port, function () {
    console.log("Server is running on "+ port +" port");
})
app.use(express.json())
app.get('/api/guests/', function (req, res) {
    const list = getGuestsUseCase.get()
    console.log('LLISTA ' + list)
    res.send(list)
})

app.post('/api/guests/', function (req, res) {
    console.log('body' + req.body)
    const { email } = req.body
    const guess = {email}
    res.status(201).send(addGuestUseCase.add(req.body))
})



module.exports = app;