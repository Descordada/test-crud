import {GuestRepositoryInMemory} from "../../../../src/infrastructure/repository/GuestRepositoryInMemory"
import {describe} from "mocha"
import {expect} from 'chai'

describe('GuestRepositoryInMemory', () => {
    const guestRepositoryInMemory = new GuestRepositoryInMemory()
    it('Should be empty afer be created', () => {
        expect(guestRepositoryInMemory.get()).to.have.length(0)
    })

    it('Size should be 1 after adding a guest', () => {
        const guest = {email: "anEmail@test.com"}
        guestRepositoryInMemory.add(guest)
        const list = guestRepositoryInMemory.get()
        expect(list).to.have.length(1)
        expect(list[0].email).to.be.equal("anEmail@test.com")
    })

    it('should throw an Error if guest is null', () => {
        expect(() => guestRepositoryInMemory.add(null)).to.throw('guest could not be null')
    })

})